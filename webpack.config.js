const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    uniqueName: "mfe1"
  },
  optimization: {
    // Only needed to bypass a temporary bug
    runtimeChunk: false
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "mfe1",

      filename: "remoteEntry.js",
      exposes: {
        './Component': './src/app/app.component.ts',
        './Module': './src/app/flights/flights.module.ts'
      },
      shared: {
        "@angular/core": { singleton: true },
        "@angular/common": { singleton: true },
        "@angular/router": { singleton: true },
        "rxjs": { singleton: true },
        "@ngrx/effects": { singleton: true },
        "@ngrx/router-store": { singleton: true },
        "@ngrx/store": { singleton: true },
        "medline-mfe-store": { singleton: true }
      }
    }),
  ],
};
