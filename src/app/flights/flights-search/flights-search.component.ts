import {Component, ViewChild, ViewContainerRef, Inject, Injector, ComponentFactoryResolver} from '@angular/core';
import { AppFeature as fromApp, NotificationFeature as fromNotification } from 'medline-mfe-store/projects/medline-mfe-store/src/public-api';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-flights-search',
  templateUrl: './flights-search.component.html'
})
export class FlightsSearchComponent {
  @ViewChild('vc', { read: ViewContainerRef, static: true })
  viewContainer: ViewContainerRef;

  notifications$: Observable<fromNotification.Notification[]>;

  constructor(
    @Inject(Injector) private injector,
    @Inject(ComponentFactoryResolver) private cfr,
    private store: Store<fromApp.AppState>
    ) {
      this.notifications$ = this.store.select(fromApp.selectNotificationMessages);
    }

    search() {
      alert('Not implemented for this demo!');
    }

    async terms() {
      const comp = await import('../lazy/lazy.component').then(m => m.LazyComponent);

      const factory = this.cfr.resolveComponentFactory(comp);
      this.viewContainer.createComponent(factory, null, this.injector);
    }


    triggerEffects()
    {
      this.store.dispatch(fromNotification.spawnRandomItems());
    }
}