import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightsSearchComponent } from './flights-search/flights-search.component';
import { RouterModule } from '@angular/router';
import { FLIGHTS_ROUTES } from './flights.routes';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppFeature as fromApp } from 'medline-mfe-store/projects/medline-mfe-store/src/public-api';
import { NotificationEffects } from 'medline-mfe-store/projects/medline-mfe-store/src/lib/notification.effects';
import { NotificationService } from 'medline-mfe-store/projects/medline-mfe-store/src/lib/notification.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FLIGHTS_ROUTES),
    StoreModule.forFeature(fromApp.FEATURE_KEY, fromApp.appReducer),
    EffectsModule.forFeature([NotificationEffects]),
  ],
  declarations: [
    FlightsSearchComponent
  ],
  providers: [
    NotificationService
  ]
})
export class FlightsModule { }


/*
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightsSearchComponent } from './flights-search/flights-search.component';
import { RouterModule } from '@angular/router';
import { FLIGHTS_ROUTES } from './flights.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FLIGHTS_ROUTES),
  ],
  declarations: [
    FlightsSearchComponent
  ]
})
export class FlightsModule { }
*/